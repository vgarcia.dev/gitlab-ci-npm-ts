# Hellonpm test module

This repository is a sample repository for
the my blog post: 

[NPM Continuous Deployment with Typescript, GitLab and Semantic Release](https://vgarcia.dev/blog/2019-11-06--publish-a-package-to-npm-step-by-step/).

>This is an in-depth guide on using GitLab CI/CD and Semantic-Release to transpile, test, version, and publish a module written in typescript to the NPM registry..

