# 1.0.0 (2019-10-28)


### Features

* allow input name ([7c353ad](https://gitlab.com/vgarcia.dev/gitlab-ci-npm-ts/commit/7c353adf3342dcd4bc83b3e12e3d3b365c4ae876))


### BREAKING CHANGES

* A new input parameter to the sayHello function
