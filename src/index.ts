const sayHello = (name: string) => `${name} says hello`;

export default sayHello;
