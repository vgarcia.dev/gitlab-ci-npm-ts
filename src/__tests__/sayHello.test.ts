import sayHello from "..";

describe("Saying hello", () => {
  it("says hello", () => {
    expect(sayHello("Pumpkin")).toBe("Pumpkin says hello");
  });
});
